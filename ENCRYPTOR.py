from Crypto.Hash import SHA256
from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES
from binascii import hexlify
import os

raw_file_path = input("Enter path to the target file : ")
public_key_path = input("Enter path to the public key file : ")
encrypted_name = input("Enter the encrypted file name : ")

raw_email = ''

#Read email from file
with open(raw_file_path, 'r') as email_file:
    raw_email = email_file.read()

#Import public key from file
public_key = RSA.import_key(open(public_key_path, 'r').read())

#Convert to bytes (To be able to use in encryption function)
raw_email_bytes = bytes(raw_email,'utf-8')

#Hash the message for message integrity check
hashing = SHA256.new()
hashing.update(raw_email_bytes)
header = hashing.hexdigest()

#Generate random 256 bit AES key
AES_Key = os.urandom(16)

#Encrypt payload message by using AES in CFB mode
iv = b'0000000000000000'
AESEncryptor = AES.new(AES_Key, AES.MODE_CFB, IV=iv)
payload = AESEncryptor.encrypt(raw_email_bytes)
payload = hexlify(payload).decode()

#Encrypt AES key by using public key
OEAPEncryptor = PKCS1_OAEP.new(key=public_key)
signature = OEAPEncryptor.encrypt(AES_Key)
signature = hexlify(signature).decode()

#Serialize string
encryptedData = header + '.' + payload + '.' + signature

#Save the encrypted file
with open('encrypted/'+ encrypted_name, 'w') as encrypted_email_file:
    encrypted_email_file.write(encryptedData)

print('Your file has been encrypted!')