from Crypto.Hash import SHA256
from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES
from binascii import hexlify, unhexlify
import os

encrypted_file_path = input("Enter path to the encrypted file : ")
private_key_path = input("Enter path to the private key : ")

#Read encrypted email from the file
encryptedData = ''
with open(encrypted_file_path, 'r') as encrypted_email_file:
    encryptedData = encrypted_email_file.read()

#Read private key from file
private_key = RSA.import_key(open(private_key_path, 'r').read())

#Extract serialized message
splittedData = encryptedData.split('.')

header = splittedData[0]
payload = splittedData[1]
signature = splittedData[2]

#Convert string to bytes
sinatureByte = unhexlify(bytes(signature,'utf-8'))
payloadByte = unhexlify(bytes(payload,'utf-8'))

#Decrypt AES key with private key
OAEPDecryptor = PKCS1_OAEP.new(key=private_key)
decryptedAESKey = OAEPDecryptor.decrypt(sinatureByte)

#Decrypt payload by the decrypted AES key
iv = b'0000000000000000'
AESDecryptor = AES.new(decryptedAESKey, AES.MODE_CFB, IV=iv)
decryptedEmail = AESDecryptor.decrypt(payloadByte)

#Hash decrypted payload
hashing = SHA256.new()
hashing.update(decryptedEmail)
hashed_payload = hashing.hexdigest()

print('Decrypted email : \n')
print(decryptedEmail.decode())

#Check message integrity by compare the orginal hash with the hashed payload
print('\nhash check :', hashed_payload == header)