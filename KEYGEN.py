from Crypto.PublicKey import RSA

#Receive file name from the user
file_name = input("Enter key name here : ")

private_key = RSA.generate(1024) # Generate 1024 bits RSA private key
public_key = private_key.publickey() # Generate public key from the existing private key

private_key_str = private_key.export_key().decode() # Export private key as String
public_key_str = public_key.export_key().decode() # Export public key as String

print('\nYour public key is \n')
print(public_key_str)

 # Store private key as file
with open( 'key_store/' + file_name, 'w') as privte_key_file:
    privte_key_file.write(private_key_str)

 # Store public key as file    
with open('key_store/' + file_name + '.pub', 'w') as public_key_file:
    public_key_file.write(public_key_str)
